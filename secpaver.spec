
Name: secpaver
Summary: Security policy development tool
Version: 1.0.2
Release: 6
License: MulanPSL-2.0
URL: https://gitee.com/openeuler/secpaver
Source: https://gitee.com/openeuler/secpaver/repository/archive/%{name}-%{version}.tar.gz
#source1 version sys@v0.0.0-20220908164124-27713097b956
Source1: sys.tar.gz

Patch0001: dont-do-daemon-reload-in-make-install.patch
Patch0002: set-default-log-path.patch
%ifarch riscv64
Patch1000: 1000-fix-build-on-riscv64.patch
%endif

BuildRequires: golang make systemd
Requires: %{name}-plugin = %{version}-%{release}

%define  debug_package %{nil}

%description
Security policy development tool

%package selinux
Summary: SELinux plugin for secPaver
License: MuLan PSL v2
BuildRequires: libselinux-devel libsepol-devel libsemanage-devel
Requires: %{name} = %{version}-%{release}
Requires: libselinux libsepol libsemanage checkpolicy policycoreutils
Provides: %{name}-plugin = %{version}-%{release}

%description selinux
SELinux plugin for secPaver.

%prep
%autosetup -n %{name}-%{version} -p1
%ifarch loongarch64
rm -rf vendor/golang.org/x/sys
tar -xf %{SOURCE1} -C vendor/golang.org/x/
%endif

%build
%make_build everything

%install
%make_install DESTDIR="%{buildroot}"

%check

%files
%defattr(0600,root,root,0700)
%attr(0700,root,root) %{_bindir}/pav
%attr(0700,root,root) %{_bindir}/pavd
%dir %{_sysconfdir}/secpaver
%dir %{_sysconfdir}/secpaver/pavd
%dir %{_libdir}/secpaver
%dir %{_datadir}/secpaver
%dir %{_datadir}/secpaver/scripts
%dir %{_localstatedir}/local/secpaver
%dir %{_localstatedir}/local/secpaver/policies
%dir %{_localstatedir}/local/secpaver/projects
%config(noreplace) %{_sysconfdir}/secpaver/pavd/config.json
%{_unitdir}/pavd.service

%files selinux
%defattr(0600,root,root,0700)
%attr(0700,root,root) %{_libdir}/secpaver/selinux.so
%dir %{_localstatedir}/local/secpaver/policies/selinux
%dir %{_datadir}/secpaver/scripts/selinux
%{_datadir}/secpaver/scripts/selinux/config
%attr(0700,root,root) %{_datadir}/secpaver/scripts/selinux/*.sh

%post
%systemd_post pavd.service

%preun
%systemd_preun pavd.service

%postun
%systemd_postun_with_restart pavd.service

%changelog
* Fri Sep 15 2023 misaka00251 <liuxin@iscas.ac.cn> - 1.0.2-6
- Fix build on riscv64

* Tue Jul 11 2023 huajingyun <huajingyun@loongson.cn> - 1.0.2-5
- Add loong64 support

* Mon Jan 16 2023 yangchenguang <yangchenguang@uniontech.com> - 1.0.2-4
- add buildrequires systemd for macros.systemd %{_unitdir}

* Tue Aug 16 2022 fushanqing <fushanqing@kylinos.cn> - 1.0.2-3
- Unified license name specification

* Thu Sep 30 2021 luhuaxin <1539327763@qq.com> - 1.0.2-2
- set default log path

* Thu Sep 30 2021 luhuaxin <1539327763@qq.com> - 1.0.2-1
- Package init
